Futtatási opciók:

A három futtatási opciót egymás után kell használni. 

Paraméteroptimalizáció
  Paraméteroptimalizációt végez a megadott évhez tartozó adatokhoz négy optimalizációs módszerrel:
  - Markowitz-módszer
  - kibővített Markowitz-módszer
  - Shortfall-módszer
  - kibővített Shortfall-módszer

  Parancs:
  python optimizer.py year=<év> symbols='<szimbólum lista>'

  Paraméterek (minden paraméter megadása kötelező):
  year: az az év, aminek az árfolyamadatait szeretnénk használni.
  Példa használat: year=2012
  symbols: azoknak a részvényszimbólumoknak a listája, amikből az optimalizáció során a portfóliót állítjuk össze.
  Példa használat: symbols='AA,FTI'
 
  Egy 'save_hyperopt.pkl' fájlt állít elő. Ezt a portfólió-optimalizáció használja.

Portfólió-optimalizáció
  
  Parancs:
  python optimizer.py 'load_hyperopt'

  Egy 'plot_data.pkl' fájlt állít elő. Ez az ábrák készítéséhez kell.

Ábrák készítése
  Parancs:
  python optimizer.py 'plot'

  Előállított ábrák:
  'sortino.png': a portfólió-optimalizációkhozoz tartozó Sortino-arányok
  'mdd.png': a portfólió-optimalizációkhozoz tartozó bevétel/Maximum drawdown arányok
  'cumulative_returns.png': a portfólió-optimalizációkhozoz tartozó bevételek
  

