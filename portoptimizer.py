# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da

# Third Party Imports
import datetime as dt
import pandas as pd
import numpy as np
import cvxpy as cvx
from cvxpy.error import SolverError
import glpk
from cvxpy import *
from numpy.linalg import *
from pyquery import PyQuery as pq
from lxml import etree
from scipy import ndimage
import urllib
import ystockquote
import sys
import copy as cp
from scipy import ndimage
import time
import pickle

class FailedIterations(Exception):
    pass

def date_range(start_date,end_date):
    if isinstance(start_date, dt.datetime):
        start_date = start_date.date()
    if isinstance(end_date, dt.datetime):
        end_date = end_date.date()

    while True:
        yield start_date

        start_date = start_date + dt.timedelta(days=1)

        if start_date > end_date:
            break

def showProgress(i,n):
    sys.stdout.write('\r')
    percent =  int(i * 100.0/(n - 1))
    sys.stdout.write("[%s] %d%%" % ('=' * percent, percent))
    sys.stdout.flush()
    if n-1 == i:
        print


def filter_symbols(ls_symbols,num_symbols=100,allowed_sectors=[]):
    sectors = {}

    try:
        pkl_file = open('sectors.pkl', 'rb')
        sectors = pickle.load(pkl_file)
        pkl_file.close()
    except IOError as e:
        sectors = {}

    base_url = "https://finance.yahoo.com/q/in?s="
    labels = []

    print 'Filtering industry data'

    i = 0
    while i < len(ls_symbols):
        ticker_symbol = ls_symbols[i]
        #print 'symbol: ' + ticker_symbol

        showProgress(i,len(ls_symbols))

        if ticker_symbol in sectors:
            if sectors[ticker_symbol] == '':
                ls_symbols.remove(ticker_symbol)
            else:
                i = i + 1
            continue

        try:
            d = pq(url=base_url + ticker_symbol + '+Industry')
            sector = d('th:contains("Sector:")').siblings().children()[0].text.strip()
            industry = d('th:contains("Industry:")').siblings().children()[0].text.strip()
            if allowed_sectors:
                if sector in allowed_sectors:
                    sectors[ticker_symbol] = sector
                    i = i + 1
                else:
                    ls_symbols.remove(ticker_symbol)
                    sectors[ticker_symbol] = ''
            else:
                sectors[ticker_symbol] = sector
                i = i + 1

            print industry

        except IndexError,err:
            ls_symbols.remove(ticker_symbol)
            sectors[ticker_symbol] = ''

    print

    try:
        pkl_file = open('sectors.pkl', 'wb')
        pickle.dump(sectors, pkl_file)
        pkl_file.close()
    except IOError as e:
        print e

    ls_symbols = ls_symbols[0:min(num_symbols,len(ls_symbols))]
    if len(ls_symbols) < num_symbols:
        print 'Only ' + str(len(ls_symbols)) + ' symbols'

    return ls_symbols


def getIndustries(ls_symbols):
    sectors = None
    try:
        pkl_file = open('sectors.pkl', 'rb')
        sectors = pickle.load(pkl_file)
        pkl_file.close()
    except IOError as e:
        sectors = {}

    base_url = "https://finance.yahoo.com/q/in?s="
    labels = []

    print 'Downloading industry data'
    i = 0
    for ticker_symbol in ls_symbols:
        showProgress(i,len(ls_symbols))
        i = i + 1

        if ticker_symbol in sectors:
            labels.append((sectors[ticker_symbol],''))
            continue

        d = pq(url=base_url + ticker_symbol + '+Industry')
        sector = d('th:contains("Sector:")').siblings().children()[0].text
        #industry = d('th:contains("Industry:")').siblings().children()[0].text
        labels.append((sector,''))

    print
    return labels

def buy_and_hold(na_all_data,all_closing_prices,time_window_length, starting_funds = 100000, transaction_cost = 0.05):
    stocks = na_all_data.shape[1];
    time_interval_length = 30

    benchmark_portfolio = np.zeros((stocks,1))

    best_stocks = [2, 0, 16, 19]

    if best_stocks == []:
        raise Exception('Cannot simulate buy and hold strategy (too few stocks given)')

    prices = all_closing_prices[0]
    prices = prices.reshape((stocks,1))

    for stock in cp.copy(best_stocks):
        benchmark_portfolio[stock] = starting_funds / len(best_stocks)

    benchmark_portfolio = np.fix(benchmark_portfolio / (prices * (1.0 + transaction_cost) ) )

    #What part of the total money should we invest ?
    funds = [0]

    time_windows = na_all_data.shape[0]

    tw = 1
    for time_window in range(time_window_length,time_windows):
        tw = tw + 1

        na_data = na_all_data[time_window - time_window_length:min(time_window, na_all_data.shape[0] )][:]
        closing_prices = all_closing_prices[time_window - time_window_length:min(time_window, na_all_data.shape[0] )][:]

        #daily_returns = np.mean(na_data,axis=0);
        daily_returns = na_all_data[time_window]

        prices = closing_prices[-1] #Melyik napra vonatkozo arat hasznaljuk (mikor vasarolunk) ?
        #prices = np.mean(closing_prices,axis=0);

        prices = prices.reshape((stocks,1))

        funds_temp = funds[-1] + float(np.dot(daily_returns,benchmark_portfolio * prices))
        funds.append(funds_temp)

    #Starting funds is the same amount that we have invested at all times
    funds = map(lambda e: e + starting_funds,funds)
    return funds


'''
Optimization based on the QSTK library
'''
def qstkopt(na_data,gamma = 0):
    '''Function gets a 100 sample point frontier for given returns'''

    # Special Case with fTarget=None, just returns average rets.
    (na_avgrets, na_std, b_error) = tsu.OptPort(na_data, None)

    # Declaring bounds on the optimized portfolio
    na_lower = np.zeros(na_data.shape[1])
    na_upper = np.ones(na_data.shape[1])

    # Getting the range of possible returns with these bounds
    (f_min, f_max) = tsu.getRetRange(na_data, na_lower, na_upper,
                            na_avgrets, s_type="long")

    # Getting the step size and list of returns to optimize for.
    f_step = (f_max - f_min) / 100.0
    lf_returns = [f_min + x * f_step for x in range(101)]

    # Declaring empty lists
    lf_std = []
    lna_portfolios = []

    # Calling the optimization for all returns
    '''
    for f_target in lf_returns:
        (na_weights, f_std, b_error) = tsu.OptPort(na_data, f_target,
                                na_lower, na_upper, s_type="long")
        lf_std.append(f_std)
        lna_portfolios.append(na_weights)

    return (lf_returns, lf_std, lna_portfolios, na_avgrets, na_std)
    '''

    (na_weights, f_std, b_error) = tsu.OptPort(na_data)

    return na_weights

'''
Returns a CVXPY expression representing the variance based risk
'''
def get_variance_risk(na_data,x):
    #Calculate S (covariance matrix)

    mean = np.mean(na_data,axis=0)
    temp = na_data - mean
    datapoints = na_data.shape[0]
    S = None
    if datapoints > 1:
        S = np.dot(temp.T,temp) / (datapoints - 1)
    else:
        S = np.dot(temp.T,temp)

    # Return x'Sx
    return quad_form(x,S)

'''
Returns a CVXPY expression representing an estimation of shortfall
'''
def get_shortfall_risk(na_data,x,daily_returns_mean,constraints,alpha=1):
    datapoints = na_data.shape[0]
    stocks = na_data.shape[1]
    K = np.floor(alpha * datapoints)
    sh = Variable(datapoints)
    for i in range(0,datapoints):
        constraints.append((na_data[i,:] * x)/datapoints == sh[i])
    return (np.matrix(daily_returns_mean) * x) - 1.0/K * (np.ones((1,datapoints)) * sh)


def get_pre_variance_risk(na_data,x_pre):
    # Gets an array of floats
    def from_matrix_to_1darray(mtx):
        return mtx.A1

    x_pre_arr = from_matrix_to_1darray(x_pre)

    #Calculate S (covariance matrix)

    mean = np.mean(na_data,axis=0)
    temp = na_data - mean
    datapoints = na_data.shape[0]
    S = None
    if datapoints > 1:
        S = np.dot(temp.T,temp) / (datapoints - 1)
    else:
        S = np.dot(temp.T,temp)

    return float(np.dot(np.dot(x_pre_arr.T,S),x_pre_arr))


def get_pre_shortfall_risk(na_data,x_pre,alpha=1):
    # Gets an array of floats
    def from_matrix_to_1darray(mtx):
        return mtx.A1

    x_pre_arr = from_matrix_to_1darray(x_pre)
    datapoints = na_data.shape[0]
    stocks = na_data.shape[1]
    K = np.floor(alpha * datapoints)
    daily_returns_mean = np.mean(na_data,axis=0)
    return np.dot(daily_returns_mean,x_pre) - 1.0/K * np.sum(np.dot(na_data,x_pre),axis=1)


def Optimize(na_all_data,all_closing_prices,starting_day = 0,gamma = 0,delta = 0,epsilon = 0,zeta = 0,time_interval_length = None,\
        shorting = True, sector_intervals = None,solver_options = {},\
        start_time = None,x_pre = None,start_portfolio = None, shorting_constraint = np.power(np.e,5), starting_funds = 100000,\
        transaction_cost = 0.05,opt_type='convopt',risk_type='variance',alpha=0.5,max_iters=1000,max_failed_iters=None):
    #Calculate time interval and iterations

    '''
    time_windows = 1
    if time_interval_length == None:
        time_interval_length = na_all_data.shape[0]
    else:
        time_windows = int(np.ceil(float(na_all_data.shape[0]) / time_interval_length))
    '''
    time_windows = na_all_data.shape[0]

    #This is going to be the x portfolio vector in the previous time window
    stocks = na_all_data.shape[1];

    #Beginning portfolio
    if x_pre == None or start_portfolio == None:
            x_pre = np.matrix( np.zeros((stocks,1)) )
            start_portfolio = np.zeros(stocks)

    risks = []
    returns = []
    transactions = []
    portfolios = [start_portfolio]
    # Only cash
    funds = [starting_funds]
    # All funds in cash + all money invested in stocks
    total_funds = []
    present_value_of_investment = [0]
    x_s = [x_pre]

    tw = 0

    sector_lengths = map(lambda i: (sector_intervals + [stocks])[i+1]\
        - (sector_intervals + [stocks])[i],[i for i in range(len(sector_intervals))])

    failed_iterations = 0
    for time_window in range(time_interval_length,time_windows):
        print 'time_window is ' + str(time_window)
        '''
        if start_time:
            print("---Runtime is %s seconds ---" % str(time.time() - start_time))
            print 'Time window ' + str(tw) + ' of ' + str(time_windows)
            time.sleep(2)
        '''

        #showProgress(i,time_windows)
        tw = tw + 1

        '''
        na_data = na_all_data[(time_window * time_interval_length):\
            min( ((time_window + 1) * time_interval_length), na_all_data.shape[0] )][:]
        '''
        na_data = na_all_data[time_window - time_interval_length:min(time_window, na_all_data.shape[0] )][:]

        # Do the optimization
        #Calculate daily returns

        daily_returns_mean = np.matrix(np.mean(na_data,axis=0))

        #Specify variables

        x_long = None
        x_short = None

        if shorting:
            x_long = Variable(stocks)
            x_short = Variable(stocks)

        x = Variable(stocks)
        x_d = Variable(stocks)

        constraints = []

        constraints = [np.ones((1,stocks)) * x == 1,\
            x == x_pre + x_d]

        y = []
        z = 0

        #Setting constraints for 1-2 norm
        if sector_intervals != None:
            z = Variable(len(sector_intervals))

            constraints.append(z >= 0)

            for i in range(len(sector_intervals)):

                num = None
                if i+1 == len(sector_intervals):
                    num = stocks - sector_intervals[i]
                    y.append(Variable(num))
                    constraints.append(x[sector_intervals[i]:stocks] == y[i])
                else:
                    num = sector_intervals[i+1] - sector_intervals[i]
                    y.append(Variable(num))
                    constraints.append(x[sector_intervals[i]:sector_intervals[i+1]] == y[i])

                constraints.append( cvx.square( cvx.pos( np.sqrt(sector_lengths[i]) * cvx.norm(y[i],2) - z[i] ) ) <= 0)

        if shorting:
            constraints.append(x_long >= 0)
            constraints.append(x_short >= 0)
            constraints.append(x == x_long - x_short)
            constraints.append(shorting_constraint >= x)
            constraints.append(x >= - (shorting_constraint))
        else:
            constraints.append(x >= 0)

        risk = None
        if risk_type == 'variance':
            risk = get_variance_risk(na_data,x)
        if risk_type == 'shortfall':
            risk = get_shortfall_risk(na_data,x,daily_returns_mean,constraints,alpha=alpha)

        objective = None
        try:
            objective = Minimize(risk - gamma * (np.matrix(daily_returns_mean) * x) + delta * cvx.norm(x,1)\
            + epsilon * cvx.norm(z,1) + zeta * cvx.norm(x_d,1))
        except ValueError as e:
            pass

        problem = Problem(objective,constraints)

        result = None
        exc = None

        #Try SCS solver first
        #result = problem.solve(solver=SCS,verbose=False,solver_specific_opts={'MAX_ITERS': 1000000,'EPS': 1e-1})
        try:
            result = problem.solve(solver=SCS,verbose=False,max_iters = max_iters,eps=1e-3)
        except SolverError as e:
            exc = e

        if problem.status != cvx.OPTIMAL or exc:
            try:
                result = problem.solve(solver=CVXOPT,verbose=False,max_iters = max_iters)
            except SolverError as e:
                exc = e

        if problem.status != cvx.OPTIMAL or exc:
            exc = None
            try:
                result = problem.solve(solver=ECOS,verbose=False,max_iters = max_iters)
            except SolverError as e:
                exc = e

        x_value = None

        if problem.status == cvx.OPTIMAL_INACCURATE or problem.status == cvx.INFEASIBLE_INACCURATE\
            or problem.status == cvx.UNBOUNDED_INACCURATE or x.value == None:
            print 'Problem is ' + str(problem.status) + ', using last portfolio'
            x_value = x_s[-1]
            failed_iterations = failed_iterations + 1

            if max_failed_iters and failed_iterations >= max_failed_iters:
                raise FailedIterations
        else:
            x_value = x.value

        # Gets an array of floats
        def from_matrix_to_1darray(mtx):
            return mtx.A1

        def matrix_map(l,mtx):
            temp = np.matrix(np.zeros(mtx.shape))
            for i in range(mtx.shape[0]):
                for j in range(mtx.shape[1]):
                    temp[i,j] = l(mtx[i,j])
            return temp

        # Smooths calculation errors
        def smooth(x_value):
            if shorting:
                return x_value
            else:
                x_value = matrix_map(lambda e: 0 if e < 0 else e,x_value)
                x_value = x_value / np.sum(x_value)
                return x_value

        # Calculate potential funds
        portfolio = cp.copy(portfolios[-1])
        closing_prices_yesterday =  all_closing_prices[time_window-1]
        invested_funds = sum(portfolio * closing_prices_yesterday)
        not_invested_funds = funds[-1]
        potential_funds = invested_funds + not_invested_funds

        # Calculate potential transactions
        x_value = smooth(x_value)

        potential_portfolio = np.fix( from_matrix_to_1darray(x_value) * potential_funds / closing_prices_yesterday )
        potential_transactions = potential_portfolio - portfolio
        if not shorting:
            assert (len([i for i in list(potential_portfolio) if i < 0]) == 0 )


        def get_sells(potential_transactions):
            return np.array(map(lambda e: e if e < 0 else 0,potential_transactions))

        def get_buys(potential_transactions):
            return np.array(map(lambda e: e if e > 0 else 0,potential_transactions))

        # TODO: how to implement shorting ?
        # Calculate sold stocks
        sells = get_sells(potential_transactions)
        portfolio = portfolio + sells
        not_invested_funds = not_invested_funds + sum(np.abs(sells) * closing_prices_yesterday) * (1 - transaction_cost)
        assert isinstance(not_invested_funds,float)

        # Limit potential buys to the amount of money we have
        potential_buys = get_buys(potential_transactions)
        potential_price = sum(potential_buys * closing_prices_yesterday) * (1 + transaction_cost)
        buys = 0
        # If potential_price == 0, we don't want to buy anything
        if potential_price != 0:
            buys = np.floor(potential_buys * not_invested_funds / potential_price)

        # Adjust portfolio and current funds
        portfolio += buys
        not_invested_funds -= sum(buys * closing_prices_yesterday)

        # Save data
        funds.append(not_invested_funds)
        portfolios.append(portfolio)
        x_pre = x_s[-1] # Is this good ?

        x_s.append(x_value)
        risk = None
        if risk_type == 'variance':
            risk = get_pre_variance_risk(na_data,x_pre)
        if risk_type == 'shortfall':
            risk = get_pre_shortfall_risk(na_data,x_pre,alpha=alpha)
        risks.append(risk)
        total_funds.append(not_invested_funds + sum(portfolio * closing_prices_yesterday))

    print 'Failed iterations: ' + str(failed_iterations)
    return (risks,total_funds,x_s)


def getDateInterval(endyear = 2012,endmonth = 1, endday = 1,days_delta = 365):
    dt_end = dt.datetime(endyear, endmonth, endday)
    dt_start = dt_end - dt.timedelta(days=days_delta)
    return (dt_start,dt_end)


def getDataQSTK(ls_symbols = [],endyear = 2012,endmonth = 1, endday = 1,days_delta = 365,\
        interpolate = True, time_interval_length = 30, workday_intervals = True):

    # Creating an object of the dataaccess class with Yahoo as the source.
    c_dataobj = da.DataAccess('Yahoo')

    ls_all_syms = c_dataobj.get_all_symbols()

    ls_symbols = ls_symbols if ls_symbols else cp.copy(ls_all_syms)

    # Bad symbols are symbols present in portfolio but not in all syms
    ls_bad_syms = list(set(ls_symbols) - set(ls_all_syms))
    for s_sym in ls_bad_syms:
        i_index = ls_symbols.index(s_sym)
        ls_symbols.pop(i_index)

    # Start and End date of the charts
    dt_end = dt.datetime(endyear, endmonth, endday)
    dt_start = dt_end - dt.timedelta(days=days_delta)

    # We need closing prices so the timestamp should be hours=16.
    dt_timeofday = dt.timedelta(hours=16)

    # Get a list of trading days between the start and the end.
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
    ld_timestamps = list(date_range(dt_start,dt_end))

    #True if the time_interval_length is given in trading days instead of normal days
    if workday_intervals:
        date_intervals = []
    else:
        #Select only interval starts
        date_interval_starts = [ld_timestamps[i*time_interval_length]\
            for i in range(int(len(ld_timestamps) / time_interval_length))]

        date_intervals = [(date_interval_starts[i],date_interval_starts[i+1]) for i in range(len(date_interval_starts)-1)]
        ldt_dates = map(lambda ts: ts.date(),ldt_timestamps)

        dates = map(lambda intv: len([d for d in ldt_dates if d >= intv[0] and intv[1] > d]) ,date_intervals)

        date_intervals = [0] + dates
        date_intervals = [sum(date_intervals[0:(i+1)]) for i in range(len(date_intervals)) ]

    # Reading just the close prices
    df_close = c_dataobj.get_data(ldt_timestamps, ls_symbols, "close")

    na_data = None

    #Do a linear interpolation
    #Doesn't work
    #if interpolate:
    #    df_close.interpolate(axis=0)


    # Filling the data for missing NAN values
    df_close = df_close.fillna(method='ffill')
    df_close = df_close.fillna(method='bfill')

    # Copying the data values to a numpy array to get returns
    na_data = df_close.values.copy()
    closing_prices = df_close.values.copy()

    # Getting the daily returns
    tsu.returnize0(na_data)

    def nansbefore(data):
        count = 0
        for x in data:
            if not x == np.nan or not x == nan:
                    break
        count = count + 1

        return (0,count)

    def nansafter(data):
        rang = nansbefore(data[::-1])
        return (data.shape[0] - rang[1],data.shape[0])

    def extrapolate(na_data):
        for i in range(na_data.shape[1]):
            data = na_data[:,i]

            b_nan_range = nansbefore(data)
            f_nan_range = nansafter(data)

            notnans = data[b_nan_range[1]:f_nan_range[0]]
            x = range(len(data))
            fit = np.polyfit(x[b_nan_range[1]:f_nan_range[0]], notnans, 3)
            line = np.poly1d(fit)

            if (not b_nan_range == (0,0)):
                na_data[b_nan_range[0]:b_nan_range[1],i] = line(x[b_nan_range[0]:b_nan_range[1]])

            if (not f_nan_range == (data.shape[0],data.shape[0])):
                na_data[f_nan_range[0]:f_nan_range[1],i] = line(x[f_nan_range[0]:f_nan_range[1]])

        return na_data

    extrapolate(na_data)
    extrapolate(closing_prices)

    for i in range(len(na_data)):
        for j in range(len(na_data[0])):
            if np.isnan(na_data[i][j]):
                #raise Exception('nan in input data !')
                #Ideiglenes megoldas...
                if j == 0:
                    na_data[i][j] = 0
                    closing_prices[i][j] = 0
                else:
                    na_data[i][j] = na_data[i][j-1]
                    closing_prices[i][j] = closing_prices[i][j-1]


    return (na_data,closing_prices,map(lambda ts: str(ts.date()),ldt_timestamps),date_intervals)

def getDataYstock(ls_symbols,startdate = '2013-01-01',enddate='2014-01-01'):
    data = np.zeros((1,1))
    i = 0
    #Hogyan lehet megoldani, hogy minden reszvenynel minden naphoz levo elemet kitoltsunk valamivel ?
    for ticker in ls_symbols:
        j = 0
        closing_prices = np.array()
        # Datum szerinti sorrend ?
        daily_infos = ystockquote.get_historical_prices(ticker, startdate, enddate).values()
        for day in daily_infos:
            closing_prices[j][i] = (float(day['Close']))
            ++j

        ++i

    nans = np.isnan(data)
    #Is this filling good ?
    data[nans] = 0

    return data.tolist()



def sortBySector(ls_symbols,na_data,closing_prices):
    industries = getIndustries(ls_symbols)

    getSector = lambda item: item[0];
    sectors = map(getSector,industries)
    temp = zip(sectors,ls_symbols,na_data.T.tolist(),closing_prices.T.tolist())

    def compare(a,b):
        if a[0] < b[0]:
            return -1
        elif a[0] > b[0]:
            return 1
        else:
            return 0

    temp = sorted(temp,cmp=compare)

    getSymbol = lambda item: item[1]
    getData = lambda item: item[2]
    getClosingPrices = lambda item: item[3]

    sectors = map(getSector,temp)
    sector_intervals = [sectors.count(e) for e in np.unique(sectors)]
    sector_intervals = [sum(sector_intervals[0:i]) for i in range(len(sector_intervals))]

    sectors = np.unique(sectors).tolist()

    ls_symbols = map(getSymbol,temp)
    na_data = np.array(map(getData,temp)).T
    closing_prices = np.array(map(getClosingPrices,temp)).T

    return (sectors,ls_symbols,na_data,closing_prices,sector_intervals)


