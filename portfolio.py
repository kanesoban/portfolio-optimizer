import numpy as np

class Portfolio:
    """Class for handling portfolio related data"""

    def __init__(self,stocks_num,closing_prices,initial_non_invested_funds,shorting = False,transaction_percent = 0,day=0,debt_limit=0):
        self.stocks_num = stocks_num
        self.closing_prices = closing_prices
        self.non_invested_funds = [initial_non_invested_funds]
        self.non_invested_funds_today = initial_non_invested_funds
        self.non_invested_funds_yesterday = None
        self.shorting = shorting
        self.transaction_percent = transaction_percent
        self.day = day
        self.debt_limit = debt_limit

    def next_day():
        self.day = self.day + 1
        self.non_invested_funds_yesterday = self.non_invested_funds_today
        self.non_invested_funds.append(self.non_invested_funds_yesterday)

    '''
        num_sells : numpy array with only negative elements
    '''
    def sell(num_sells):
        #Sell array can only have negative elements
        assert not [e for e in num_sells if e > 0]

        def has_neg_stock(stocks_num):
            return bool([stock_num for stock_num in stocks_num if stock_num < 0])

        if transaction_percent > 0:
            raise Exception('Not implemented function: sell/shorting')
        else:
            asser
            temp = self.stocks_num + num_sells
            #If short selling is not allowed, we cannot have negative number of stocks
            if not self.shorting and has_neg_stock(temp)
                raise Exception('Cannot have negative number of stocks !')

            self.non_invested_funds_today = self.non_invested_funds + sum(self.closing_prices[self.day-1] * abs(num_sells))
            self.stocks_num = temp

    def buy(num_buys):
        #Buy array can only have positive elements
        assert not [e for e in num_sells if e < 0]

        if transaction_percent > 0:
            raise Exception('Not implemented function: sell/shorting')
        else:
            temp = self.non_invested_funds - sum(self.closing_prices[self.day-1] * num_buys)
            #We cannot go into more debt then our limit
            if temp < self.debt_limit:
                raise Exception('Debt limit is ' + str(self.debt_limit) + '/' + str(temp))

            self.non_invested_funds_today = temp
            self.stocks_num = self.stocks_num + num_buys
