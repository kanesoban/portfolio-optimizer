# -*- coding: utf-8 -*-

from __future__ import unicode_literals

# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da

import time
import copy
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from pyquery import PyQuery as pq
from numpy import *
from numpy.linalg import *
import datetime as dt
import pylab
from pylab import *
import multiprocessing as mp

from scipy import ndimage

import portoptimizer as opt

import pickle

import sys
import traceback
import re

import random as rnd

from matplotlib.ticker import Formatter, FixedLocator, FixedFormatter

from hyperopt import fmin, tpe, hp, STATUS_OK, Trials

from matplotlib2tikz import save as tikz_save

import matplotlib.lines as lines

import googlefinance as gf

from pudb import set_trace; set_trace()

SHORTING = False
FUNDS = 100000
LS_SYMBOLS = 500
TOTAL_DAYS = 365
ENDYEAR = 2011
ENDMONTH = 1
ENDDAY = 1
MAX_EVALS = 100
MAX_ITERS = 10000
MAX_FAILED_ITERS=10
OPT_SHORT_DESC = {
    'gamma' : 'Markowitz optimization'
}

OPTIMIZATION_KEYS = ['gamma','gamma_delta_epsilon_zeta','shortfall-gamma','shortfall-gamma_delta_epsilon_zeta']

OPTIMIZATION_DESCRIPTIONS = {
    'gamma' : 'Markowitz-módszer',
    'gamma_delta_epsilon_zeta' : 'Kibovített Markowitz-módszer',
    'shortfall-gamma' : 'Shortfall-módszer',
    'shortfall-gamma_delta_epsilon_zeta' : 'Kibovített Shortfall-módszer'
}

#SNP500 20 symbols in 4 sectors
LS_SYMBOLS_SUBSET = ['AA', 'FTI', 'CLF', 'SWN', 'IFF', 'MO', 'HAR', 'OI', 'MKC', 'KMB', 'STJ', 'BCR', 'XRAY', 'UNH', 'THC', 'WY', 'BCR', 'ADP', 'MYL', 'MRK']

parameters_dic = {
        'gamma' : ['time_interval_length','gamma'],
        'gamma_delta' : ['time_interval_length','gamma','delta'],
        'gamma_epsilon' : ['time_interval_length','gamma','epsilon'],
        'gamma_zeta' : ['time_interval_length','gamma','zeta'],
        'gamma_delta_epsilon' : ['time_interval_length','gamma','delta','epsilon'],
        'gamma_delta_zeta' : ['time_interval_length','gamma','delta','zeta'],
        'gamma_epsilon_zeta' : ['time_interval_length','gamma','epsilon','zeta'],
        'gamma_delta_epsilon_zeta' : ['time_interval_length','gamma','delta','epsilon','zeta'],
        'shortfall-gamma' : ['time_interval_length','gamma','alpha'],
        'shortfall-gamma_delta_epsilon_zeta' : ['time_interval_length','alpha','gamma','delta','epsilon','zeta']
}

TRAIN_RATIO = 0.6
TRAINING_DAYS = TRAIN_RATIO*TOTAL_DAYS

PROGRAM_START_TIME = time.time()

DT_END = dt.datetime(ENDYEAR,ENDMONTH,ENDDAY)
TRAINING_END_DATE = DT_END - dt.timedelta(days=(1-TRAIN_RATIO)*TOTAL_DAYS)

ALLOWED_SECTORS=[]

def main5(optimizations = {}, ls_symbols = [], na_all_data = [], tr_len = 0, sector_intervals = [], closing_prices = [], buy_and_hold_funds = [], use_training_set = False):

    start_time = time.time()


    markowitz_key = optimizations.keys()[0]
    time_interval_length = optimizations[markowitz_key]['time_interval_length']
    buy_and_hold_funds = opt.buy_and_hold(na_all_data[tr_len - time_interval_length:,:],all_closing_prices[tr_len - time_interval_length:,:],time_interval_length)


    results = {}
    for key in optimizations:
        time_interval_length = optimizations[key]['time_interval_length']

        risk_type = 'variance'
        m = re.search('shortfall',key)
        if m:
            risk_type = 'shortfall'
        if use_training_set:
            results[key] = opt.Optimize(na_all_data[0:tr_len,:], closing_prices[0:tr_len,:], shorting = SHORTING, sector_intervals = sector_intervals, time_interval_length = optimizations[key]['time_interval_length'],\
                    gamma = optimizations[key]['gamma'], delta = optimizations[key]['delta'], epsilon = optimizations[key]['epsilon'], zeta = optimizations[key]['zeta'],\
                    start_time = start_time, starting_funds = FUNDS, risk_type=risk_type, max_iters=MAX_ITERS*10)
        else:
            results[key] = opt.Optimize(na_all_data[tr_len - time_interval_length:,:], closing_prices[tr_len - time_interval_length:,:], shorting = SHORTING, sector_intervals = sector_intervals, time_interval_length = optimizations[key]['time_interval_length'],\
                    gamma = optimizations[key]['gamma'], delta = optimizations[key]['delta'], epsilon = optimizations[key]['epsilon'], zeta = optimizations[key]['zeta'],\
                    start_time = start_time, starting_funds = FUNDS,risk_type=risk_type,max_iters=MAX_ITERS*10)


    ''', x_pre = optimizations[key]['last_portfolio']'''

    #Calculate overall value for every type of optimization

    returns = {}
    returns_neg = {}

    values = []

    for key in results:
        res = results[key][1]
        last = len(res)
        returns[key] = [(res[i] - res[i-1]) for i in range(1,last)]
        returns_neg[key] = [ret for ret in returns[key] if ret < 0]

        sig = np.sqrt(ndimage.variance(np.array(returns_neg[key])))
        if sig == 0 or np.isnan(sig):
            sig = 1

        values.append( np.mean( returns[key] ) / sig )


    res = buy_and_hold_funds
    last = len(res)
    buy_and_hold_returns = [(res[i] - res[i-1]) for i in range(1,last)]
    buy_and_hold_returns_neg = [ret for ret in buy_and_hold_returns if ret < 0]
    sig = np.sqrt(ndimage.variance(np.array(buy_and_hold_returns_neg)))
    if sig == 0 or np.isnan(sig):
        sig = 1

    buy_and_hold_values = np.mean( buy_and_hold_returns ) / sig

    dic = { 'optimizations' : optimizations, 'values' : values, 'results' : results, 'buy_and_hold_returns' : buy_and_hold_funds,\
            'buy_and_hold_values' :  buy_and_hold_values}
    try:
        print 'Saving plot_data.pkl'
        filename = 'plot_data.pkl'
        pkl_file = open(filename, 'wb')
        pickle.dump(dic, pkl_file)
        pkl_file.close()
    except IOError as e:
        print e



def plot_only(use_training_set = False):
    dic = {}
    try:
        filename = 'plot_data.pkl'
        pkl_file = open(filename, 'rb')
        dic = pickle.load(pkl_file)
        pkl_file.close()
    except IOError as e:
        print str(e)

    optimizations = dic['optimizations']
    results = dic['results']
    buy_and_hold_funds = dic['buy_and_hold_returns']
    values = dic['values']

    returns = {}

    for key in results:
        res = results[key][1]
        last = len(res)
        returns[key] = [(res[i] - res[i-1]) for i in range(1,last)]


    res = buy_and_hold_funds
    last = len(res)
    buy_and_hold_returns = [(res[i] - res[i-1]) for i in range(1,last)]
    buy_and_hold_returns_neg = [ret for ret in buy_and_hold_returns if ret < 0]
    sig = np.sqrt(ndimage.variance(np.array(buy_and_hold_returns_neg)))
    if sig == 0 or np.isnan(sig):
        sig = 1

    buy_and_hold_values = np.mean( buy_and_hold_returns ) / sig

    '''
    NOTE: these results are not going to be accurate because in the buy and hold case we are using
    non negative variance
    '''

    plot_sortino(optimizations, values, buy_and_hold_values,use_training_set = use_training_set)
    plot_cumulative_returns(optimizations, results, buy_and_hold_funds,use_training_set = use_training_set)
    plot_mdd(optimizations,results,buy_and_hold_funds,use_training_set = use_training_set)


def plot_sortino(optimizations, mean_values, buy_and_hold_values, use_training_set = False):
    plt.clf()
    columns = ('time window', 'gamma', 'delta', 'epsilon', 'zeta')

    opt_keys = optimizations.keys()
    color_range = np.linspace(0.2, 1.0, len(opt_keys) + 1)
    color_idx = np.linspace(1, len(opt_keys) + 1, len(opt_keys) + 1)
    colors = plt.cm.nipy_spectral(color_range)

    rects = {}
    i = 1
    bar_width = 0.4

    plt.figure(figsize=(14, 8),dpi=200)
    plt.title('Optimalizációs módszerek a ' + str(ENDYEAR - 1) + ' évben')
    gs = gridspec.GridSpec(2, 1, height_ratios=[7, 3])
    ax1 = plt.subplot(gs[0])

    for opt_key in optimizations:
        rects[opt_key] = ax1.bar(i, mean_values[i-1], bar_width,
          alpha=0.4,
          color=colors[i-1],
          label=OPTIMIZATION_DESCRIPTIONS[opt_key])
        i = i + 1

    ax1.bar(i, buy_and_hold_values, bar_width,
          alpha=0.4,
          color=colors[len(optimizations.keys())],
          label='Buy and hold módszer')

    lgd = ax1.legend(bbox_to_anchor=(1.1, 1), loc='lower left', borderaxespad=0.,fontsize=16)

    ax1.set_xlabel('Optimalizációs módszer',fontsize=20)

    ax1.set_ylabel('Sortino-arány',fontsize=20)

    plt.tight_layout()

    plt.xticks([])


    filename = 'sortino.png'
    if use_training_set:
        filename = 'sortino.png'
    plt.savefig(filename, format='png', bbox_extra_artists=(lgd,), bbox_inches='tight')


def plot_returns_distribution(optimizations, results, buy_and_hold_returns, use_training_set = False):

    opt_keys = optimizations.keys()

    color_range = np.linspace(0.2, 1.0, len(opt_keys) + 1)
    color_idx = np.linspace(1, len(opt_keys) + 1, len(opt_keys) + 1)
    colors = plt.cm.nipy_spectral(color_range)

    i = 1
    for opt_key in opt_keys:
        plt.clf()

        gs = gridspec.GridSpec(2, 1, height_ratios=[6, 4])
        ax1 = plt.subplot(gs[0])

        plt.title('Distribution of daily returns of optimization ' + str(i))

        lgd = ax1.legend(bbox_to_anchor=(1.1, 1), loc=0, borderaxespad=0.)
        ax1.set_ylabel('Number of returns')

        res_max = np.max(results[opt_key])
        res_min = np.min(results[opt_key])
        bins = np.linspace(-res_max,res_max,20).tolist()
        ax1.hist(results[opt_key], bins, normed=0, histtype='bar', rwidth=0.8, color = colors[i-1])

        make_hyperopt_table(optimizations,colors = colors, color_idx = color_idx, ax = ax2)

        pylab.savefig('returns_distribution' + str(i) + '.png', format='png', bbox_inches='tight')
        i = i + 1

    plt.clf()

    gs = gridspec.GridSpec(2, 1, height_ratios=[6, 4])
    ax1 = plt.subplot(gs[0])

    plt.title('Distribution of daily returns of buy and hold strategy')

    lgd = ax1.legend(bbox_to_anchor=(1.1, 1), loc=0, borderaxespad=0.)
    ax1.set_ylabel('Number of daily returns in dollars')

    res_max = np.max(results[opt_key])
    res_min = np.min(results[opt_key])
    bins = np.linspace(-res_max,res_max,20).tolist()

    ax1.hist(buy_and_hold_returns, bins, normed=0, histtype='bar', rwidth=0.8, color = colors[i-1])

    filename = 'returns_distribution' + str(i) + '.png'
    if use_training_set:
        filename = 'returns_distribution' + str(i) + '.png'
    pylab.savefig(filename, format='png', bbox_inches='tight')


def get_ret_mdd(returns):
    mdd = 0
    for i in range(len(returns)):
        peak = returns[i]
        for j in range(i+1,len(returns)):
            if (returns[i] - returns[j]) > mdd:
                mdd = returns[i] - returns[j]
    return mdd

#Plot maximum drawdown
def plot_mdd(optimizations, results, buy_and_hold_returns, use_training_set=False ):
    plt.clf()

    opt_keys = optimizations.keys()
    color_range = np.linspace(0.2, 1.0, len(opt_keys) + 1)
    color_idx = np.linspace(1, len(opt_keys) + 1, len(opt_keys) + 1)
    colors = plt.cm.nipy_spectral(color_range)

    rects = {}
    i = 1
    bar_width = 0.4

    plt.figure(figsize=(14, 8),dpi=200)
    plt.title('Optimalizációs módszerek a ' + str(ENDYEAR - 1) + ' évben')
    gs = gridspec.GridSpec(2, 1, height_ratios=[7, 3])
    ax1 = plt.subplot(gs[0])

    for opt_key in optimizations:
        returns_mdd = get_ret_mdd(results[opt_key][1])
        rects[opt_key] = ax1.bar(i, [np.mean(results[opt_key][1]) / returns_mdd], bar_width,
          alpha=0.4,
          color=colors[i-1],
          label=OPTIMIZATION_DESCRIPTIONS[opt_key])
        i = i + 1
    
    returns_mdd = get_ret_mdd(buy_and_hold_returns)
    rects[opt_key] = ax1.bar(i, [np.mean(buy_and_hold_returns) / returns_mdd], bar_width,
        alpha=0.4,
        color=colors[i-1],
        label='Buy and hold módszer')

    lgd = ax1.legend(bbox_to_anchor=(1.1, 1), loc='lower left', borderaxespad=0.,fontsize=16)

    ax1.set_xlabel('Optimalizációs módszerek',fontsize=20)

    ax1.set_ylabel('Átlagos bevételek/Maximum drawdown',fontsize=20)

    plt.tight_layout()
 
    plt.xticks([])
    filename = 'mdd.png'
    plt.savefig(filename, format='png', bbox_extra_artists=(lgd,), bbox_inches='tight')

def make_boxplots(optimizations, returns, buy_and_hold_returns, use_training_set = False):
    def setBoxColors(i,bp,color):
        setp(bp['boxes'][i], color=color)
        setp(bp['caps'][2*i], color=color)
        setp(bp['caps'][2*i + 1], color=color)
        setp(bp['whiskers'][2*i], color=color)
        setp(bp['whiskers'][2*i + 1], color=color)
        setp(bp['fliers'][i], color=color)
        setp(bp['medians'][i], color=color)
        flier = bp['fliers'][i]
        flier.set(marker='')

    plt.clf()

    opt_keys = optimizations.keys()

    returns_plus = []

    for opt_key in opt_keys:
        returns_plus.append(returns[opt_key])

    returns_plus.append(buy_and_hold_returns)

    color_range = np.linspace(0.2, 1.0, len(opt_keys) + 1)
    color_idx = np.linspace(1, len(opt_keys) + 1, len(opt_keys) + 1)
    colors = plt.cm.nipy_spectral(color_range)

    bp = plt.boxplot(returns_plus)
    names = [str(i) for i in range(8)]
    names.append('Buy and hold módszer')
    setp(names)

    lgd = plt.legend(bbox_to_anchor=(1.1, 1), loc=0, borderaxespad=0.)

    for i in range(len(opt_keys) + 1):
        setBoxColors(i,bp,colors[i])

    plt.xticks( range(0,10), ('', '1', '2', '3', '4', '5', '6', '7', '8', '9') )

    plt.title('Variation of returns')

    ax = plt.gca()
    ax.set_xlabel('Optimization methods')
    ax.set_ylabel('Returns')

    filename = 'returns_boxplot.png'
    if use_training_set:
        filename = 'returns_boxplot.png'
    pylab.savefig(filename, format='png')

def plot_cumulative_returns(optimizations, results, buy_and_hold_returns, use_training_set = False):
    plt.clf()
    opt_keys = optimizations.keys()
    color_range = np.linspace(0.2, 1.0, len(opt_keys) + 1)
    color_idx = np.linspace(1, len(opt_keys) + 1, len(opt_keys) + 1)
    colors = plt.cm.nipy_spectral(color_range)
    linestyles = ['-', '--', ':']
    markers = ['*','+','.','o','x','v']

    plt.figure(figsize=(20, 12),dpi=200)

    gs = gridspec.GridSpec(2, 1, height_ratios=[10, 3])
    ax1 = plt.subplot(gs[0])

    opt_keys = optimizations.keys()
    for i in range(len(opt_keys)):
        if i < len(linestyles):
            ax1.plot(results[opt_keys[i]][1],color=colors[i],
                linestyle=linestyles[i],
                label=OPTIMIZATION_DESCRIPTIONS[opt_keys[i]])
        else:
            marker = markers[(i - len(linestyles)) % len(markers)]
            ax1.plot(results[opt_keys[i]][1],color=colors[i],
                marker=marker,
                label=OPTIMIZATION_DESCRIPTIONS[opt_keys[i]])

    last_marker = (i - len(linestyles)) % len(markers)
    last = len(opt_keys)
    marker = markers[last_marker]

    ax1.plot(buy_and_hold_returns,marker=marker,color=colors[last],label='Buy and hold módszer')
    lgd = ax1.legend(bbox_to_anchor=(1.05, 1), loc='lower left', borderaxespad=0.,fontsize=16)

    plt.xticks([])

    dt_interval = opt.getDateInterval(endyear = ENDYEAR,days_delta = TOTAL_DAYS)
    ax1.set_xlabel('Pénzmennyiség alakulása 100 nap alatt',fontsize=20)
    ax1.set_ylabel('Teljes pénz mennyisége dollárban',fontsize=20)

    filename = 'cumulative_returns.png'
    if use_training_set:
        filename = 'cumulative_returns.png'
    plt.savefig(filename,format='png', bbox_extra_artists=(lgd,), bbox_inches='tight')

def plot_alpha():
    pass

def plot_treynor(optimizations, results, na_data, closing_prices):
    # Get the mean of closing prices

    r_b = np.mean(closing_prices,axis=1)

    # Normalize benchmark
    r_b = r_b / r_b[0]

    # Returnize benchmark
    r_b[1:] = r_b[1:] / r_b[0:-1] - 1

    alphas = {}
    treynor_ratios = {}
    for key in results:
        # Calculate returns
        temp = np.array(results[key][1])
        temp = temp / temp[0]
        # Mas szamítasi mod: eloszor a beveteleket kellene atlagolni, aztan returnize ?
        r_p = np.ones(len(temp))
        r_p[1:] = temp[1:] / temp[0:-1] - 1

        # Use regression (least squares) to find alpha, beta
        A = np.vstack([r_b,np.ones(len(r_b))]).T
        beta, alpha = np.linalg.lstsq(A,r_p)[0]

        # Calculate alpha, Treynor ratio
        alphas[key] = alpha
        treynor_ratios[key] = np.mean(r_p) / beta

        print 'Alpha of ' + key + ' is ' + str(alpha)
        print 'Treynor ratio of ' + key + ' is ' + str(treynor_ratios[key])


def plot_correlation_square(na_all_data, sector_intervals, filename=None, sectors=None, use_training_set = False):
    plt.clf()
    R = corrcoef(na_all_data.T)
    R = R * R
    R_rev = R[::-1]

    fig = plt.gcf()
    ax = plt.gca()

    ax.xaxis.tick_top()

    figure_title = 'Correlation matrix for S&P 500 stocks for ' + str(TOTAL_DAYS) + ' number of days'
    text_height = 1.0
    if sectors:
        ax.xaxis.set_major_formatter(FixedFormatter(sector_intervals))
        ax.xaxis.set_major_locator(FixedLocator(sector_intervals))
        ax.yaxis.set_major_formatter(FixedFormatter(sector_intervals))
        ax.yaxis.set_major_locator(FixedLocator(sector_intervals))

        text_height = 1.08
        ax.set_xticklabels(sectors,rotation=-45)
        ax.set_yticklabels(sectors)
        plt.figure(figsize=(10, 10),dpi=200)

        plt.text(0.5, text_height, figure_title,
            horizontalalignment='center',
            fontsize=20)
    else:
        plt.title(figure_title)
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    plt.pcolor(R_rev)
    plt.colorbar()

    for i in range(1,len(sector_intervals)):
        line1 = [(sector_intervals[i],LS_SYMBOLS), (sector_intervals[i],0)]
        line2 = [(0,LS_SYMBOLS - sector_intervals[i]), (LS_SYMBOLS,LS_SYMBOLS - sector_intervals[i])]

        (line1_xs, line1_ys) = zip(*line1)
        (line2_xs, line2_ys) = zip(*line2)

        ax.add_line(Line2D(line1_xs, line1_ys, linewidth=2, color='white'))
        ax.add_line(Line2D(line2_xs, line2_ys, linewidth=2, color='white'))

    pylab.savefig(filename + str('.png'), format='png')



def make_hyperopt_table(optimizations,colors = [],color_idx = [], figname = None, ax = None):
    data = []

    keys = ['time_interval_length', 'gamma', 'delta', 'epsilon', 'zeta']

    row_labels = optimizations.keys()

    i = 0

    for opt_key in optimizations:
        data.append([])

        for key in keys:
            data[i].append(optimizations[opt_key][key])

        i = i + 1

    columns = ('time window', 'gamma', 'delta', 'epsilon', 'zeta')

    # Get some pastel shades for the colors
    if colors == []:
        colors = plt.cm.nipy_spectral(np.linspace(0, 1.0, len(row_labels)))

    make_table(data,columns = columns,rows = [('          ' + str(x)) for x in range(1,len(row_labels) + 1)],colors = colors[0:8],\
               figname = figname, ax = ax)


def make_table(data,columns = [],rows = [],ylabel = '',colors = [],\
                  figname = None, ax = None):
    n_rows = len(data)

    index = np.arange(len(columns)) + 0.3
    bar_width = 0.4

    cell_text = []
    for row in range(n_rows):
        row_text = [str(data[row][0])]
        for x in data[row][1:]:
            if x - int(x) == 0:
                row_text.append('%1.1f' % x)
            else:
                row_text.append('{:e}'.format(x))
        cell_text.append(row_text)
    cell_text.reverse()

    if figname:
        plt.clf()

    # Add a table at the bottom of the axes
    if ax:
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        the_table = ax.table(cellText=cell_text,
            rowLabels=rows,
            rowColours=colors,
            colLabels=columns,
            loc='center')
    else:
        the_table = plt.table(cellText=cell_text,
        rowLabels=rows,
        rowColours=colors,
        colLabels=columns,
        loc='bottom')

    # Adjust layout to make room for the table:
    plt.subplots_adjust(left=0.2, bottom=0.2)

    if figname:
        plt.savefig(figname,format='png')

def sortino_loss(returns):
    #Get negative returns. We want to punish negative variance
    returns_neg = [ret for ret in returns if ret < 0]
    variance = np.sqrt(ndimage.variance(np.array(returns_neg)))
    if variance == 0 or np.isnan(variance):
        variance = 1

    return variance / np.mean( returns )

#Maximum drawdown loss
def mdd_loss(returns):
    mdd = 0
    for i in range(len(returns)):
        peak = returns[i]
        for j in range(i+1,len(returns)):
            if (returns[i] - returns[j]) > mdd:
                mdd = returns[i] - returns[j]
    return mdd


def hyperoptimize(na_all_data = [], closing_prices = [],max_evals = 100,sector_intervals = [], parameters = [],\
                  time_interval_length = None, gamma = None, risk_type = 'variance'):
    all_params = ['time_interval_length','gamma','delta','epsilon','zeta']
    not_opt_params = [x for x in all_params if not x in parameters]
    iterations = 0

    def objective_function(params):
        global iterations

        try:
            iterations += 1
        except NameError:
            iterations = 0

        if 'time_interval_length' in params:
            params['time_interval_length'] += 3

            if params['time_interval_length'] >= na_all_data.shape[0]:
                return {
                    'loss': float('inf'),
                    'status': STATUS_OK,
                    'last_portfolio': None
                }

        for param in not_opt_params:
            if param == 'time_interval_length':
                params[param] = time_interval_length
            elif param == 'gamma':
                params[param] = gamma
            else:
                params[param] = 0

        try:
            print 'Iteration ' + str(iterations + 1) + ' of ' + str(MAX_EVALS)
            results = opt.Optimize(na_all_data, closing_prices, shorting = SHORTING,sector_intervals = sector_intervals, starting_funds = FUNDS, risk_type = risk_type, 
                max_iters = MAX_ITERS,max_failed_iters=MAX_FAILED_ITERS,**params)
            print 'Done. Runtime is ' + str(time.time() - PROGRAM_START_TIME)
        except opt.FailedIterations as e:
            return {
                'loss' : float('inf'),
                'status' : "FAILED_ITERATIONS"
            } 
        except Exception as e:
            print(traceback.format_exc())
            return {
                'loss': float('inf'),
                'status': STATUS_OK,
                'last_portfolio': None
            }

        returns = results[1]

        return {
            'loss': LOSS_FUNCTION( returns ),
            'status': STATUS_OK,
            'last_portfolio': results[2][-1]
        }


    if 'time_interval_length' in parameters:
        space = [('time_interval_length','unif',1,40)]
    else:
        space = []

    for param in parameters:
        if param == 'time_interval_length':
            continue
        if param == 'alpha':
            space.append( (param,'unif_float',0,1) )
            continue
        space.append( (param,'exp',-10,10) )

    eval_params = []
    results = []
    minloss = float("inf")
    last_portfolio = None
    best = {}
    for i in range(max_evals):
        while True:
            temp = {}
            for intv in space:
                if intv[1] == 'exp':
                    temp[intv[0]] = np.exp(rnd.randint(intv[2],intv[3]))
                if intv[1] == 'unif':
                    temp[intv[0]] = rnd.randint(intv[2],intv[3])
                if intv[1] == 'unif_float':
                    randnum = rnd.uniform(intv[2],intv[3])
                    while randnum == intv[2] or randnum == intv[3]:
                        randnum = rnd.uniform(intv[2],intv[3])
                    temp[intv[0]] = randnum

            res = objective_function(temp)
            if res['status'] != STATUS_OK or res['loss'] == float('inf'):
                continue

            if minloss > res['loss']:
                minloss = res['loss']
                best['last_portfolio'] = res['last_portfolio']
                for param in parameters:
                    best[param] = temp[param]
            break

    for param in all_params:
        if not param in parameters:
            best[param] = 0

    if time_interval_length and gamma:
        best['time_interval_length'] = time_interval_length
        best['gamma'] = gamma
    else:
        best['time_interval_length'] = best['time_interval_length'] + 3

    return best

def save_optimizations(optimizations,na_all_data,ls_symbols,tr_len,sector_intervals,closing_prices):
    try:
        pkl_file = open('save_hyperopt.pkl', 'wb')
        save_hyperopt = {
                        'optimizations': optimizations,
                        'na_all_data' : na_all_data,
                        'ls_symbols' : ls_symbols,
                        'tr_len' : tr_len,
                        'sector_intervals' : sector_intervals,
                        'closing_prices' : closing_prices
        }
        pickle.dump(save_hyperopt, pkl_file)
        pkl_file.close()
    except IOError as e:
        print str(e)

def do_hyperoptimizations():

    c_dataobj = da.DataAccess('Yahoo')

    ls_symbols = None

    if LS_SYMBOLS_SUBSET:
        ls_symbols = LS_SYMBOLS_SUBSET
    else:
        ls_symbols = c_dataobj.get_all_symbols()
    ls_symbols = opt.filter_symbols(ls_symbols,num_symbols=LS_SYMBOLS)
 
    print 'Number of filtered symbols: ' + str(len(ls_symbols))

    (na_all_data,closing_prices,trading_days,arr) = opt.getDataQSTK(ls_symbols = ls_symbols,days_delta = TOTAL_DAYS)

    (sectors,ls_symbols,na_all_data,closing_prices,sector_intervals) = opt.sortBySector(ls_symbols,na_all_data,closing_prices)

    #First TRAIN_RATIO % is training data, (1-TRAIN_RATIO)% is test data
    tr_len = int(TRAIN_RATIO * na_all_data.shape[0])

    optimizations = {}

    output = mp.Queue()
    def hyperoptimize0(opt_key,args):
        res = hyperoptimize(**args)
        output.put((opt_key,res))

    processes = []

    i = 0
    for key in OPTIMIZATION_KEYS:
        m = re.search('shortfall',OPTIMIZATION_KEYS[i])
        risk_type = 'variance'
        if m:
            risk_type = 'shortfall'

        args = {
            'na_all_data' : na_all_data[0:tr_len,:],
            'closing_prices' : closing_prices[0:tr_len,:],
            'max_evals' : MAX_EVALS,
            'sector_intervals' : sector_intervals,
            'parameters' : parameters_dic[key],
            'risk_type' : risk_type
        }

        processes.append(mp.Process(target=hyperoptimize0, args=(key,args)))
        i = i + 1

    for i in range(0,len(OPTIMIZATION_KEYS)):
        try:
            if not OPTIMIZATION_KEYS[i] in optimizations or optimizations[OPTIMIZATION_KEYS[i]] == 'ERROR':
                processes[i].start()
            print 'start ' + str(i)
        except Exception as e:
            optimizations[OPTIMIZATION_KEYS[i]] = 'ERROR'
            print(traceback.format_exc())
            raise e

    for i in range(0,len(OPTIMIZATION_KEYS)):
        print 'join ' + str(i)
        processes[i].join()

    for i in range(0,len(OPTIMIZATION_KEYS)):
        print 'get ' + str(i)
        res = output.get()
        optimizations[res[0]] = res[1]

    return optimizations


def find_benchmark_companies(biggest_n = 10):

    c_dataobj = da.DataAccess('Yahoo')

    ls_symbols = None

    if LS_SYMBOLS_SUBSET:
        ls_symbols = LS_SYMBOLS_SUBSET
    else:
        ls_symbols = c_dataobj.get_all_symbols()
    ls_symbols = opt.filter_symbols(ls_symbols,num_symbols=LS_SYMBOLS)
 
    print 'Filtered symbols: ' + str(len(ls_symbols))

    (na_all_data,all_closing_prices,trading_days,arr) = opt.getDataQSTK(ls_symbols = ls_symbols,days_delta = TRAINING_DAYS,endyear = TRAINING_END_DATE.year,\
        endmonth = TRAINING_END_DATE.month, endday = TRAINING_END_DATE.day)

    (sectors,ls_symbols,na_all_data,all_closing_prices,sector_intervals) = opt.sortBySector(ls_symbols,na_all_data,all_closing_prices)

    time_windows = na_all_data.shape[0]

    #This is going to be the x portfolio vector in the previous time window
    stocks = na_all_data.shape[1];

    prices = all_closing_prices[0]
    prices = prices.reshape((stocks,1))

    portfolio = np.ones((stocks,1)) * 1000 / prices

    risks = []
    returns = []
    transactions = []
    funds = np.ones((stocks,1)) * 1000

    tw = 1
    for time_window in range(1,time_windows):
        tw = tw + 1

        na_all_data = na_all_data[time_window:min((time_window + 1), na_all_data.shape[0] )][:]
        closing_prices = all_closing_prices[time_window:min((time_window + 1), na_all_data.shape[0] )][:]

        daily_returns = np.mean(na_all_data,axis=0);

        funds = funds + daily_returns * portfolio * prices

    funds = funds.tolist()
    funds = zip(funds,range(len(funds)))
    funds = sorted(funds,key=lambda e: e[0])
    biggest = funds[-1 * biggest_n:len(funds)]
    biggest = map(lambda e: e[1],biggest)
    biggest = zip(biggest,[ls_symbols[i] for i in biggest])

    print biggest


def shorting_optimization():
    c_dataobj = da.DataAccess('Yahoo')

    ls_symbols = None

    if LS_SYMBOLS_SUBSET:
        ls_symbols = LS_SYMBOLS_SUBSET
    else:
        ls_symbols = c_dataobj.get_all_symbols()
    ls_symbols = opt.filter_symbols(ls_symbols,num_symbols=LS_SYMBOLS)
 
    print 'Filtered symbols: ' + str(len(ls_symbols))


    ls_symbols = ls_symbols[0:10]

    (na_all_data,closing_prices,trading_days,arr) = opt.getDataQSTK(ls_symbols = ls_symbols,days_delta = 200)

    (sectors,ls_symbols,na_all_data,closing_prices,sector_intervals) = opt.sortBySector(ls_symbols,na_all_data,closing_prices)

    time_windows = na_all_data.shape[0]
    powers = range(0,6)

    dic = {}

    for power in powers:
        try:
            results = opt.Optimize(na_all_data, closing_prices, shorting = SHORTING,sector_intervals = sector_intervals,\
                time_interval_length = 30, gamma = 1, delta = 1, epsilon = 1,\
                zeta = 1, starting_funds = FUNDS, shorting_constraint = np.power(np.e,power))

            returns = results[1]

            #Get negative returns. We want to punish negative variance
            returns_neg = [ret for ret in returns if ret < 0]

            variance = np.sqrt(ndimage.variance(np.array(returns_neg)))
            if variance == 0 or np.isnan(variance):
                variance = 1

            dic[power] = np.mean( returns ) / variance
        except Exception as e:
            print str(e)

    try:
        pkl_file = open('save_shorting_constraint.pkl', 'wb')
        pickle.dump(dic, pkl_file)
        pkl_file.close()
    except IOError as e:
        print str(e)



def get_parameter_constraints():
    c_dataobj = da.DataAccess('Yahoo')

    ls_symbols = None

    if LS_SYMBOLS_SUBSET:
        ls_symbols = LS_SYMBOLS_SUBSET
    else:
        ls_symbols = c_dataobj.get_all_symbols()
    ls_symbols = opt.filter_symbols(ls_symbols,num_symbols=LS_SYMBOLS)
 
    print 'Filtered symbols: ' + str(len(ls_symbols))

    ls_symbols = ls_symbols[0:10]

    (na_all_data,closing_prices,trading_days,arr) = opt.getDataQSTK(ls_symbols = ls_symbols,days_delta = 200)

    (sectors,ls_symbols,na_all_data,closing_prices,sector_intervals) = opt.sortBySector(ls_symbols,na_all_data,closing_prices)

    time_windows = na_all_data.shape[0]
    powers = range(0,11)

    param_dic = {}

    param_dic['gamma'] = {}
    for power in powers:
        try:
            results = opt.Optimize(na_all_data, closing_prices, shorting = SHORTING,sector_intervals = sector_intervals,\
                time_interval_length = 30, gamma = np.power(np.e,power), delta = 1, epsilon = 1,\
                zeta = 1, starting_funds = FUNDS, shorting_constraint = 1)

            returns = results[1]

            #Get negative returns. We want to punish negative variance
            returns_neg = [ret for ret in returns if ret < 0]

            variance = np.sqrt(ndimage.variance(np.array(returns_neg)))
            if variance == 0 or np.isnan(variance):
                variance = 1

            param_dic['gamma'][power] = np.mean( returns ) / variance
        except Exception as e:
            print str(e)

    try:
        pkl_file = open('save_parameter_constraints.pkl', 'wb')
        pickle.dump(param_dic, pkl_file)
        pkl_file.close()
    except IOError as e:
        print str(e)


    param_dic['delta'] = {}
    for power in powers:
        try:
            results = opt.Optimize(na_all_data, closing_prices, shorting = SHORTING,sector_intervals = sector_intervals,\
                time_interval_length = 30, gamma = 1, delta = np.power(np.e,power), epsilon = 1,\
                zeta = 1, starting_funds = FUNDS, shorting_constraint = 1)

            returns = results[1]

            #Get negative returns. We want to punish negative variance
            returns_neg = [ret for ret in returns if ret < 0]

            param_dic['delta'][power] = np.mean( returns ) / np.sqrt(ndimage.variance(np.array(returns_neg)))
        except Exception as e:
            print str(e)

    try:
        pkl_file = open('save_parameter_constraints.pkl', 'wb')
        pickle.dump(param_dic, pkl_file)
        pkl_file.close()
    except IOError as e:
        print str(e)

    param_dic['epsilon'] = {}
    for power in powers:
        try:
            results = opt.Optimize(na_all_data, closing_prices, shorting = SHORTING,sector_intervals = sector_intervals,\
                time_interval_length = 30, gamma = 1, delta = 1, epsilon = np.power(np.e,power),\
                zeta = 1, starting_funds = FUNDS, shorting_constraint = 1)

            returns = results[1]

            #Get negative returns. We want to punish negative variance
            returns_neg = [ret for ret in returns if ret < 0]

            param_dic['epsilon'][power] = np.mean( returns ) / np.sqrt(ndimage.variance(np.array(returns_neg)))
        except Exception as e:
            # This shorting constraint is too large ?
            print str(e)

    try:
        pkl_file = open('save_parameter_constraints.pkl', 'wb')
        pickle.dump(param_dic, pkl_file)
        pkl_file.close()
    except IOError as e:
        print str(e)

    param_dic['zeta'] = {}
    for power in powers:
        try:
            results = opt.Optimize(na_all_data, closing_prices, shorting = SHORTING,sector_intervals = sector_intervals,\
                time_interval_length = 30, gamma = 1, delta = 1, epsilon = 1,\
                zeta = np.power(np.e,power), starting_funds = FUNDS, shorting_constraint = 1)

            returns = results[1]

            #Get negative returns. We want to punish negative variance
            returns_neg = [ret for ret in returns if ret < 0]

            param_dic['zeta'][power] = np.mean( returns ) / np.sqrt(ndimage.variance(np.array(returns_neg)))
        except Exception as e:
            print str(e)

    try:
        pkl_file = open('save_parameter_constraints.pkl', 'wb')
        pickle.dump(param_dic, pkl_file)
        pkl_file.close()
    except IOError as e:
        print str(e)


def make_correlation_plot():
    c_dataobj = da.DataAccess('Yahoo')

    ls_symbols = None

    if LS_SYMBOLS_SUBSET:
        ls_symbols = LS_SYMBOLS_SUBSET
    else:
        ls_symbols = c_dataobj.get_all_symbols()
    ls_symbols = opt.filter_symbols(ls_symbols,num_symbols=LS_SYMBOLS)
 
    print 'Number of filtered symbols: ' + str(len(ls_symbols))

    ls_symbols = ls_symbols[0:min(LS_SYMBOLS,len(ls_symbols))]

    (na_all_data,all_closing_prices,trading_days,arr) = opt.getDataQSTK(ls_symbols = ls_symbols,days_delta = TOTAL_DAYS)

    (sectors,ls_symbols,na_all_data,all_closing_prices,sector_intervals) = opt.sortBySector(ls_symbols,na_all_data,all_closing_prices)

    plot_correlation_square(na_all_data,sector_intervals,filename='correlation_mosaic_plot')

def get_hyperopt_arguments():
    for arg in sys.argv[1:]:
        #Get a list of opt. parameters
        value = arg.split('=')[1]

        if re.search('year', arg):
            start_year = int(value)
            global ENDYEAR
            ENDYEAR = start_year + 1

        if re.search('symbols', arg):
            global LS_SYMBOLS_SUBSET
            LS_SYMBOLS_SUBSET = value.split(',')

    return optimizations


def getSNP500data():
    c_dataobj = da.DataAccess('Yahoo')

    ls_symbols = None

    if LS_SYMBOLS_SUBSET:
        ls_symbols = LS_SYMBOLS_SUBSET
    else:
        ls_symbols = c_dataobj.get_all_symbols()
    ls_symbols = opt.filter_symbols(ls_symbols,num_symbols=LS_SYMBOLS)
 
    print 'Number of filtered symbols: ' + str(len(ls_symbols))

    (na_all_data,all_closing_prices,trading_days,arr) = opt.getDataQSTK(ls_symbols = ls_symbols,days_delta = TOTAL_DAYS, endyear=ENDYEAR,endday = ENDDAY,\
        endmonth = ENDMONTH)

    return opt.sortBySector(ls_symbols,na_all_data,all_closing_prices)

def getNASDAQdata():
    def compare_industry(a,b):
        if a[2] < b[2]:
            return -1
        elif a[2] > b[2]:
            return 1
        else:
            return 0

    get_symbol = lambda item: item[0]
    get_sector = lambda item: item[1]
    get_industry = lambda item: item[2]


    def nansbefore(data,nans=[]):
        count = 0
        for x in data:
            if not (np.isnan(x) or x in nans):
                break
        count = count + 1

        return (0,count)

    def nansafter(data,nans=[]):
        rang = nansbefore(data[::-1],nans=nans)
        return (data.shape[0] - rang[1],data.shape[0])

    def extrapolate(na_data,nans=[]):
        for i in range(na_data.shape[1]):
            data = na_data[:,i]

            b_nan_range = nansbefore(data,nans=nans)
            f_nan_range = nansafter(data,nans=nans)

            notnans = data[b_nan_range[1]:f_nan_range[0]]
            x = range(len(data))
            fit = np.polyfit(x[b_nan_range[1]:f_nan_range[0]], notnans, 3)
            line = np.poly1d(fit)

            if (not b_nan_range == (0,0)):
                na_data[b_nan_range[0]:b_nan_range[1],i] = line(x[b_nan_range[0]:b_nan_range[1]])

            if (not f_nan_range == (data.shape[0],data.shape[0])):
                na_data[f_nan_range[0]:f_nan_range[1],i] = line(x[f_nan_range[0]:f_nan_range[1]])

        return na_data

    # Forward fill
    def ffill(data,nans=[]):
        for i in range(data.shape[1]):
            if np.isnan(data[0][i]) or data[0][i] in nans:
                data[0][i] = 0.0
            for j in range(1,data.shape[0]):
                if np.isnan(data[j][i]) or data[j][i] in nans:
                    data[j][i] = data[j-1][i]

    # Fill backward, but only at the beginning
    def bfillb(data,nans=[]):
        for i in range(data.shape[1]):
            j = 0
            # Find first allowed number
            while( data[j][i] in nans or np.isnan(data[j][i]) ):
                 j = j + 1
            notnan = data[j][i]

            # Fill in not allowed elements at the beginning
            j = 0
            while( data[j][i] in nans or np.isnan(data[j][i]) ):
                data[j][i] = notnan
                j = j + 1

    # Get initial symbols
    ls = gf.get_symbols(stock_index='NASDAQ')
    ls_symbols = [e['symbol'] for e in ls]

    # Get closing prices and trading days
    dt_end = dt.date(ENDYEAR,ENDMONTH,ENDDAY)
    dt_start = dt_end - dt.timedelta(days=TOTAL_DAYS)

    '''
    When we try to download data for symbols, a lot of time data will be unavailable.
    We need to try to download data for more symbols until there are enough data for at least LS_SYMBOLS
    number of symbols.
    '''
    potential_symbols_num = 2*LS_SYMBOLS
    potential_symbols = ls_symbols[0:potential_symbols_num]

    (all_closing_prices,trading_days,potential_symbols) = gf.get_data(dt_start, dt_end, potential_symbols)
    while(potential_symbols_num < len(ls) and len(potential_symbols) < LS_SYMBOLS):
        (all_closing_prices,trading_days,potential_symbols) = gf.get_data(dt_start, dt_end, potential_symbols)
        potential_symbols_num += LS_SYMBOLS

    if len(all_closing_prices) == 0:
        raise Exception('all_closing_prices is empty !')

    all_closing_prices = all_closing_prices[:,0:min(len(ls_symbols),LS_SYMBOLS)]
    ls_symbols = potential_symbols[0:min(len(ls_symbols),LS_SYMBOLS)]

    # Get industries, sectors
    sectors = [e['sector'] for e in ls if e['symbol'] in ls_symbols]
    industries = [e['industry'] for e in ls if e['symbol'] in ls_symbols]

    # Sort data according to industries
    temp = zip(ls_symbols,sectors,industries)
    temp = sorted(temp,cmp=compare_industry)
    ls_symbols = [get_symbol(e) for e in temp]
    sectors = [get_sector(e) for e in temp]
    industries = [get_industry(e) for e in temp]

    # Compute sector intervals
    sector_intervals = [sectors.count(e) for e in np.unique(sectors)]
    sector_intervals = [int(sum(sector_intervals[0:i])) for i in range(len(sector_intervals))]

    # Extrapolate missing data
    nan_prices = [0,np.inf,-np.inf]
    extrapolate(all_closing_prices,nans=nan_prices)

    # Fill in missing beginning values
    ffill(all_closing_prices,nans=nan_prices)
    bfillb(all_closing_prices,nans=nan_prices)

    # Getting the daily returns
    na_all_data = copy(all_closing_prices)
    tsu.returnize0(na_all_data)

    # Extrapolate missing data
    nan_data = [np.inf,-np.inf]
    extrapolate(na_all_data,nans=nan_data)

    # Fill in missing beginning values
    ffill(na_all_data,nans=nan_data)

    # Raise exception if there are still nan values
    for i in range(len(na_all_data)):
        for j in range(len(na_all_data[0])):
            if np.isnan(na_all_data[i][j]) or na_all_data[i][j] in nan_data:
                raise Exception('NaN values are not allowed !')

    return (sectors,ls_symbols,na_all_data,all_closing_prices,sector_intervals)


def get_snp500_sectors():
    c_dataobj = da.DataAccess('Yahoo')

    ls_symbols = c_dataobj.get_all_symbols()

    base_url = "https://finance.yahoo.com/q/in?s="

    sectors = set()
    for symbol in ls_symbols:
        try:
            d = pq(url=base_url + symbol + '+Industry')
            sector = d('th:contains("Sector:")').siblings().children()[0].text
            industry = d('th:contains("Industry:")').siblings().children()[0].text
            sectors.add(sector)
        except IndexError,err:
            print str(err)

    return len(sectors)

def data_sanity_check(na_all_data,all_closing_prices):
    if 0 in na_all_data:
        return False

    if 0 in all_closing_prices:
        return False

    return True


if __name__ == '__main__':
    LOSS_FUNCTION = mdd_loss

    m = None
    save_hyperopt = None

    for arg in sys.argv:
        m = re.search('plot', arg)
        if m:
            plot_only()
            sys.exit(0)

    use_training_set = False
    for arg in sys.argv:
        m = re.search('training', arg)
        if m:
            use_training_set = True

    load_hyperopt = False
    for arg in sys.argv:
        m = re.search('load_hyperopt', arg)
        if m:
            try:
                pkl_file = open('save_hyperopt.pkl', 'rb')
                save_hyperopt = pickle.load(pkl_file)
                pkl_file.close()
                load_hyperopt = True
            except IOError as e:
                print str(e)

    optimizations = None
    na_all_data = None
    ls_symbols = None
    tr_len = None
    sector_intervals = None
    all_closing_prices = None

    # Check if we want to load hyperopt parameters from file
    if load_hyperopt:
        optimizations = save_hyperopt['optimizations']
        na_all_data = save_hyperopt['na_all_data']
        ls_symbols = save_hyperopt['ls_symbols']
        tr_len = save_hyperopt['tr_len']
        sector_intervals = save_hyperopt['sector_intervals']
        all_closing_prices = save_hyperopt['closing_prices']
    else:
        optimizations = {}
        for optimization in OPTIMIZATION_KEYS:
            optimizations[optimization] = {}
        get_hyperopt_arguments()
        (sectors,ls_symbols,na_all_data,all_closing_prices,sector_intervals) = getSNP500data()

        tr_len = int(TRAIN_RATIO * na_all_data.shape[0])
        if not optimizations.values()[0]:
            optimizations = do_hyperoptimizations()
            f = open('optimizations.txt', 'w')
            f.write(str(optimizations))
            f.close()
            save_optimizations(optimizations,na_all_data,ls_symbols,tr_len,sector_intervals,all_closing_prices)
            sys.exit()

        for arg in sys.argv:
            m = re.search('save_hyperopt', arg)
            if m:
                save_optimizations(optimizations,na_all_data,tr_len,sector_intervals,closing_prices)

    main5(optimizations = optimizations, na_all_data = na_all_data, ls_symbols = ls_symbols, tr_len = tr_len, sector_intervals = sector_intervals, closing_prices = all_closing_prices,\
        use_training_set = use_training_set)

